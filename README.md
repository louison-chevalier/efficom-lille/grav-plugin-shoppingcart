# Grav Shopping Cart Plugin

## Running Tests

### JavaScript tests

Run `cd tests/js` and then `mocha`

### PHP tests

Run `composer update` to install the testing dependencies. Then run `composer test` in the root folder
Then, run `composer update --no-dev` to uninstall the testing dependencies.

